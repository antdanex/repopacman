﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    NavMeshAgent _navAgent;
    GameObject _player;

    [SerializeField]
    GameObject GhostMesh;

    static public int Points = 200;

    void Awake()
    {
        _navAgent = GetComponent<NavMeshAgent>();
    }

    void OnDisable()
    {
        Destroy(GhostMesh);
    }

    // Use this for initialization
    void Start ()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player == null)
            throw new UnityException("Player is missing");

        _navAgent.SetDestination(_player.transform.position);
	}
	
	// Update is called once per frame
	void Update ()
    {
        GhostMesh.transform.position = transform.position;
        _navAgent.SetDestination(_player.transform.position);
    }
}
